import java.util.ArrayList;

public class Graph {

    private int[][] _matrix;
    private int _linksAmount = 0;
    

    public Graph(int length) {
        this._matrix = new int[length][length];
    }

    public void AddRoad(int from, int to, int distance) {
        this._matrix[from][to] = distance;
        this._matrix[to][from] = distance;
        this._linksAmount++;
    }

    public int GetLinksNumber() {
        return this._linksAmount;
    }

    public int GetEdgeNumber() {
        return this._matrix[0].length;
    }

    public ArrayList<Integer> GetNeighbors(int index) {
        ArrayList<Integer> neighbors = new ArrayList<>();

        for (int i = 0; i < this._matrix[index].length; i++) {
            if (this._matrix[index][i] != 0)
                neighbors.add(i);
        }

        return neighbors;
    }

    public int GetDistance(int from, int to) {
        return this._matrix[from][to];
    }

    public boolean PathExsist(int from, int to) {
        return this._matrix[from][to] != 0;
    }

    public void Print() {
        int n = this._matrix.length;
        for (int i = 1; i < n; i++) {
            System.out.println("Z " + Helpers.ANSI_RED + Helpers.CITIES[i] + Helpers.ANSI_RESET + " można dojechać do: ");
            for (int j = 1; j < n; j++) {
                if (this._matrix[i][j] == 0) {
                    continue;
                }
                System.out.println( "\t" + Helpers.ANSI_GREEN + Helpers.CITIES[j] + "(" + this._matrix[i][j] + " km) " + Helpers.ANSI_RESET);
            }
        }
    }
}