public class Heap<T extends Comparable<T>> {
    private T[] _heap;
    private int _index = 1;

    @SuppressWarnings("unchecked")
    public Heap(int size) {
        this._heap = (T[]) new Comparable[size + 1];
    }

    @SuppressWarnings("unchecked")
    public Heap(T[] arr, int n) {
        this._heap = (T[]) new Comparable[n + 1];;
        for(int i = 1; i < arr.length + 1; i++) {
            this._heap[i] = arr[i - 1];
        }
    }

    public boolean isHeap() 
	{
		int n = this._heap.length;
		for (int i = 1 ; i < n; i++) {
            T root = this._heap[i];
            int left = 2 * i;
            int right = left + 1;
            if (right < n) {
                T leftChild = this._heap[left];
                T rightChild = this._heap[right];
                
                if (root.compareTo(leftChild) <= 0 || root.compareTo(rightChild) <= 0)
                    return false;
            }
            else if (left < n) {
                T leftChild = this._heap[left];
                if (root.compareTo(leftChild) <= 0)
                    return false;
            }
            else {
                break;
            }
		}		
		return true;
    }

    public void insert(T x) {
        this._heap[_index] =  x;
        swimUp(_index);
        this._index += 1;
    }

    private void swimUp(int k) {
        while (k > 1 && compare(k/2, k)) {
            swap(k, k/2);
            k = k/2;
        }
    }

    private boolean compare(int i, int j) {
        T leftChild = this._heap[i];
        T rightChild = this._heap[j];

        return !(leftChild.compareTo(rightChild) > 0);
    }

    private void swap(int i, int j) {
		T tmp;
		tmp = this._heap[i];
        this._heap[i] = this._heap[j];
		this._heap[j] = tmp;
    }

    public void Print() {
        System.out.print(Helpers.ANSI_RED);
        for(int i = 1; i < this._heap.length; i++) {
            System.out.print(this._heap[i] + " ");
        }
        System.out.print(Helpers.ANSI_RESET);
        System.out.print("\n");
    }

    public int Height() {
        int n = this._heap.length;
        return (int) Math.ceil(Math.log((n) / Math.log(2)))  - 1;
    }
}