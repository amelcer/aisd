//package project01;
public class List<T> {

    private Node _head;
    private int _length = 0;

    private class Node {
		T value;
        Node next;
        Node prev;

        public Node(T value) {
            this.value = value;
        }
    }

    public void Add(T value) {
        this._length += 1;
        Node newNode = new Node(value);
        if (this._head == null) {
            this._head = newNode;
            this._head.prev = this._head;
            this._head.next = this._head;
            return;
        }

        Node last = GetLastNode();
        last.next = newNode;
        newNode.prev = last;
        newNode.next = this._head;
        this._head.prev = newNode;
       
    }

    private Node GetLastNode() {
        return this._head.prev;
    }
    private Node GetFirstNode() {
        return this._head;
    }

    public T Get(int index) {
        if (index > this._length) {
            return null;
        }

        Node nx = this._head;
        for (int i = 0; i < this._length; i++) {
            if (i == index) {
                return nx.value;
            }
            nx = nx.next;
        }
        return null;
    }
    
    public void AddOne(T val)
    {
    	this._length += 1;
        Node newhead = new Node(val);
        Node lastfirst = GetFirstNode();
        Node last = GetLastNode();
        this._head = newhead; 
        this._head.next = lastfirst;
        this._head.prev = last;
    	last.next = this._head;
    	lastfirst.prev=this._head;
    }
    

    public boolean Delete(T value) {
        if (this._head.value == value) {
            Node newHead = this._head.next;
            Node lastNode = this._head.prev;

            this._head = newHead;
            newHead.prev = lastNode;
            lastNode.next = this._head;
            
            this._length -= 1;
            return true;
        }
        
        Node nx = this._head;
        for (int i = 0; i < this._length; i++) {
            if (nx.value == value) {
                Node prev = nx.prev;
                Node next = nx.next;
                prev.next = next;
                next.prev = prev;
                
                this._length -= 1;
                return true;
            }
            nx = nx.next;
        }

        return false;
    }

    public int Size() {
        return this._length;
    }
    public void Print() {
        System.out.println("długość: " +  Size());
        Node nx = this._head;
        for (int i = 0; i < this._length; i++) {
            //System.out.println(nx.prev.value + " " + nx.value + " " + nx.next.value); 
            System.out.println(nx.value); 
            nx = nx.next;
        }
        System.out.println("-----------------------------------");
    }
    public void Printforone(T value) 
    {
    	Node nx = this._head;
    	while(nx.value!=value) 
    	{ 
    		nx = nx.next;
    	}
    	
    	System.out.println("Sasiedzi liczby:" + nx.value + " to " + nx.prev.value + " oraz "+ nx.next.value);
    }
}