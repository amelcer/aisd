import java.util.ArrayList;
import java.util.Comparator;

public class Zad_03 {

    static class HeapComparator implements Comparator<Heap> {
        public int compare(Heap h1, Heap h2) {
            int h1Height = h1.Height();
            int h2Height = h2.Height();

            if (h1Height < h2Height)
                return -1;
            else if (h1Height > h2Height)
                return 1;
            else
                return 0;
        }
    }
    private static ArrayList<Heap<Character>> heaps = new ArrayList<>();
    
    public static void Factorial(int n, Character[] elements) {
        if (n == 1) {
            Heap<Character> hp = new Heap<>(elements, elements.length);
            heaps.add(hp);
        } else {
            for (int i = 0; i < n-1; i++) {
                Factorial(n - 1, elements);
                if (n % 2 == 0) {
                    swap(elements, i, n-1);
                } else {
                    swap(elements, 0, n-1);
                }
            }
            Factorial(n - 1, elements);
        }
    }

    private static void swap(Character[] input, int a, int b) {
        Character tmp = input[a];
        input[a] = input[b];
        input[b] = tmp;
    }

    public static void main(String[] args) {
        Character[] signs = { 'A', 'B', 'C', 'D', 'E', 'F', 'G' };
        Factorial(signs.length, signs);
        
        Comparator cmp = new HeapComparator();
        heaps.removeIf(heap -> !heap.isHeap());
        //heaps.sort(cmp);
        heaps.forEach(heap -> {
            System.out.print("Wysokość: "+  Helpers.ANSI_RED + heap.Height() + Helpers.ANSI_RESET);
            System.out.print(" Stos: ");
            heap.Print();
            
        });
    }
}