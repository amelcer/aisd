import java.util.ArrayList;

public class Zad_02 {
    private final static int _NODES = Helpers.CITIES.length;
    private static boolean[] visited = new boolean[_NODES];
    private static ArrayList<Integer> paths = new ArrayList<>();
    private static ArrayList<ArrayList<Integer>> possiblePaths = new ArrayList<>();
    static int size = 0;
    
    static void FindAllPossiblePaths(Graph graph, int from, int to, ArrayList<Integer> avoid) {
        paths.add(from);
        size++;
        visited[from] = true;
        ArrayList<Integer> path = new ArrayList<>();

        if (from == to) {
            for (int x : paths) {
                path.add(x);
            }
            possiblePaths.add(path);
            return;
        }

        for (int neighbor : graph.GetNeighbors(from)) {
            if (!visited[neighbor] && !avoid.contains(neighbor)) {
                FindAllPossiblePaths(graph, neighbor, to, avoid);
                visited[neighbor] = false;
                size--;
                paths.remove(size);
            }
        }
    }

    static ArrayList<Integer> GetShortestPath(Graph graph) {
        ArrayList<Integer> distances = new ArrayList<>();

        possiblePaths.forEach(path -> {
            int distance = 0;
            for (int i = 0; i < path.size() - 1; i++) {
                int from = path.get(i);
                int to = path.get(i + 1);
                distance += graph.GetDistance(from, to);
            }
            distances.add(distance);
        });

        int min = distances.get(0);
        int index = -1;
        for (int i = 0; i < distances.size(); i++) {
            int distance = distances.get(i);
            if (distance < min) {
                min = distance;
                index = i;
            }
        }

        return possiblePaths.get(index);
    }
   
    public static void main(String[] args) {
        Graph roadMap = new Graph(Helpers.CITIES.length);

        roadMap.AddRoad(1, 2, 599);
        roadMap.AddRoad(1, 3, 180);
        roadMap.AddRoad(1, 4, 497);

        roadMap.AddRoad(2, 7, 420);
        roadMap.AddRoad(2, 8, 691);

        roadMap.AddRoad(3, 4, 432);
        roadMap.AddRoad(3, 5, 602);

        roadMap.AddRoad(4, 7, 345);

        roadMap.AddRoad(5, 6, 138);
        roadMap.AddRoad(5, 10, 291);

        roadMap.AddRoad(6, 7, 526);

        roadMap.AddRoad(7, 8, 440);
        roadMap.AddRoad(7, 11, 432);
        roadMap.AddRoad(7, 12, 621);

        //DENVER na obrazie ma numer 5, ale ten numer ma już Sacramento, wychodzi na to że DENVER ma numer 9
        roadMap.AddRoad(8, 9, 102);

        roadMap.AddRoad(9, 12, 452);

        roadMap.AddRoad(10, 11, 280);
        roadMap.AddRoad(10, 13, 114);

        roadMap.AddRoad(11, 14, 155);
        roadMap.AddRoad(11, 15, 108);
        roadMap.AddRoad(11, 16, 290);

        roadMap.AddRoad(12, 15, 469);
        roadMap.AddRoad(12, 19, 268);

        roadMap.AddRoad(13, 14, 138);
        roadMap.AddRoad(13, 16, 386);
        roadMap.AddRoad(13, 17, 118);

        roadMap.AddRoad(14, 15, 207);

        roadMap.AddRoad(16, 18, 116);
        roadMap.AddRoad(16, 19, 403);

        roadMap.AddRoad(17, 18, 425);

        roadMap.AddRoad(18, 19, 314);

        //roadMap.Print();
        for (int i = 0; i < _NODES; i++) {
            visited[i] = false;
        }
        
        ArrayList<Integer> citiesToAvoid = new ArrayList<>();
        citiesToAvoid.add(Helpers.GetCityIndex("Las Vegas"));
        citiesToAvoid.add(Helpers.GetCityIndex("Reno"));
        FindAllPossiblePaths(roadMap, Helpers.GetCityIndex("Los Angeles"), Helpers.GetCityIndex("Salt Lake City"), citiesToAvoid);

        ArrayList<Integer> shortestPath = GetShortestPath(roadMap);

        System.out.println("Najkrótsza droga z Los Angeles do Salt Lake City biegnie przez: ");
        shortestPath.forEach(cityIndex -> {
            System.out.println(Helpers.CITIES[cityIndex] + "(" + cityIndex + ")");
        });
    }
}