//package project01;

public class Zad_01 {
  
	public static boolean test(int n)
	{
		 for(int i = 2; i <= n / 2; i++)
		 {      
			    if (n % i == 0)
					return false;            
		 }	 
		 return true;
	}	 

	public static int FindMax(List<Integer> list) {
		int size = list.Size();
		int max = list.Get(0);
		int value;
		for (int i = 0; i < size; i++) {
			value = list.Get(i);
			if (value > max) {
				max = value;
			}
		}

		return max;
	}	
        	
   public static void main(String[] args) 
	{
		List<Integer> primes = new List<>();
		int t = 3;       	
		while(primes.Size() < 17) 
		{
			if (test(t)) 
				primes.Add(t);       			
			t++;
		}
       	t++;
        System.out.println("Podpunkt a)");
		primes.Print();
		
        System.out.println("Podpunkt b)");
        primes.AddOne(1);
        while (primes.Size()<19) 
		{
			if (test(t)) 
			{
		        primes.Add(t);       			
			}
			t++;
		}     
		primes.Print();
		
        System.out.println("Podpunkt c)");
		int max = FindMax(primes);
		primes.Delete(max);
		primes.Print();
		
    	System.out.println("Podpunkt d)");
	 	primes.Printforone(17);   
        
    }
}
