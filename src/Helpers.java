public class Helpers {
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String[] CITIES = {
        "",
        "Seattle",
        "Butte",
        "Portland",
        "Boise",
        "Sacramento",
        "Reno",
        "Salt Lake City",
        "Cheyenne",
        "Denver",
        "Bakersfield",
        "Las Vegas",
        "Albuquerque",
        "Los Angeles",
        "Barstow",
        "Kingman",
        "Phoenix",
        "San Diego",
        "Tucson",
        "El Paso"
    };

    public static int GetCityIndex(String name){
        int index = -1;
        for (int i = 1; i < CITIES.length; i++) {
            if (CITIES[i] == name) {
                index = i;
                return index;
            }
        }
        return index;
    }
}